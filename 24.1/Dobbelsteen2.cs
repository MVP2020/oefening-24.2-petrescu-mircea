﻿using System;

namespace _24._1
{
    class Dobbelsteen2 : Dobbelsteen
    {
        private int _aantalZijden;

        private int _waarde;

        private Random willGetal;

        public int AantaZijden { get; set; }

        public int Waarde { get; set; }

        public Random WIllGetal { get; set; }

        public Dobbelsteen2(int aantalZijden, Random r) : base(aantalZijden, r)
        {
            aantalZijden = 4;
        }
        public override void Roll()
        {
            Waarde = WIllGetal.Next(AantaZijden + 1);

        }
    }
}

