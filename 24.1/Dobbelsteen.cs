﻿using System;

namespace _24._1
{
    class Dobbelsteen
    {
        private int _aantalZijden;

        private int _waarde;

        private Random _willGetal;

        public int AantaZijden { get; set; }

        public int Waarde { get; set; }

        public Random WillGetal { get; set; }



        public Dobbelsteen(int aantalZijden, Random r)
        {
            AantaZijden = aantalZijden;
            WillGetal = r;
            Waarde = 0;
        }

        public virtual void Roll()
        {
            Waarde = WillGetal.Next(AantaZijden + 1);
        }

        public Dobbelsteen(Random e) : this(6, e)
        {

        }

    }
}
