﻿using System;
using System.Windows;

namespace _24._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Dobbelsteen maindice;
        Dobbelsteen dicewith4sides;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            maindice.Roll();
            dicewith4sides.Roll();
            label1.Content = maindice.Waarde.ToString();
            label2.Content = dicewith4sides.Waarde.ToString();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Random myrandom = new Random();
            maindice = new Dobbelsteen(4, myrandom);
            dicewith4sides = new Dobbelsteen(myrandom);

        }
    }
}
